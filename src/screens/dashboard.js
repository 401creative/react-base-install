import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

// Reducers
import {
  updateCurrentNav,
} from '../reducers/nav'

class Dashboard extends React.Component {
    componentDidMount() {
        this.props.updateCurrentNav('screen-dashboard')
    }
    render() {
        return (
            <div>
                <h1>Dashboard</h1>
                <p>Some text here</p>
            </div>
        );
    }
}

const mapStateToProps = state => ({
})

const mapDispatchToProps = dispatch => bindActionCreators({
  updateCurrentNav
}, dispatch)

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Dashboard)