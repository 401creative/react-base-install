export const UPDATE_CURRENT_NAV = 'counter/UPDATE_CURRENT_NAV'

const initialState = {
  currentNav: '',
}

export default (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_CURRENT_NAV:
      return {
        ...state,
        currentNav: action.className
      }

    default:
      return state
  }
}

export const updateCurrentNav = (className) => {
  return dispatch => {
    dispatch({
      type: UPDATE_CURRENT_NAV,
      className: className
    })
  }
}