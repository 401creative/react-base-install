import React from 'react'
import { push } from 'react-router-redux'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

// Reducers
import { setCurrentUser } from '../reducers/auth'
import { updateCurrentNav } from '../reducers/nav'

class Login extends React.Component {
    componentDidMount() {
        this.props.updateCurrentNav('screen-login')
    }
    doLogin(){
        this.props.setCurrentUser({id:123,user:'Cory Mensch'});
        this.props.gotoDashboard();
    }
    render() {
        return (
            <div>
                <h1>Login</h1>
                <p>Some text here</p>
                <button onClick={this.doLogin.bind(this)}>Login</button>
            </div>
        );
    }
}

const mapStateToProps = state => ({
})

const mapDispatchToProps = dispatch => bindActionCreators({
    setCurrentUser,
    updateCurrentNav,
    gotoDashboard: () => push('/dashboard')
}, dispatch)

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Login)