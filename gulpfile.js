var gulp = require('gulp');
var $ = require('gulp-load-plugins')();

var onError = function (err) {
    $.util.beep();
    console.log(err.toString());
    this.emit('end');
};

gulp.task('css', function() {
    return gulp.src('src/sass/style.scss')
        .pipe($.plumber({errorHandler: onError}))
        .pipe($.sourcemaps.init())
        .pipe($.sass.sync({outputStyle: 'compressed', includePaths: require('bourbon').includePaths}))
        .pipe($.autoprefixer())
        .pipe($.sourcemaps.write())
        .pipe(gulp.dest('src/css'))
        .pipe($.notify('SASS Compiled and Minified!  (っ◕‿◕)っ'));
});

gulp.task('watch',function() {
    gulp.watch('src/sass/**/*.*ss',['css']);
});