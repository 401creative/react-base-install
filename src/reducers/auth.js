export const SET_CURRENT_USER = 'SET_CURRENT_USER';

const initialState = {
  isAuthenticated: false,
  user: {}
};

export default (state = initialState, action = {}) => {
  switch(action.type) {
    case SET_CURRENT_USER:
      return {
        ...state,
        isAuthenticated: (action.user?true:false),
        user: action.user
      };
    default: return state;
  }
}

export const setCurrentUser = (user) => {
  return dispatch => {
    dispatch({
      type: SET_CURRENT_USER,
      user: user
    })
  }
}