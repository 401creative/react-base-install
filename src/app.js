import React from 'react';
import { Route, Link, withRouter } from 'react-router-dom'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

// Utils
import { requireAuth } from './utils/requireAuth'

// Screens
import Login from './screens/login'
import Dashboard from './screens/dashboard'

class App extends React.Component {
    render(){
        let user = false;
        if(this.props.user){
            user = (<p>{this.props.user.id}{this.props.user.user}</p>);
        }
        return (
            <div className={this.props.currentNav}>
                <header>
                  <Link className={this.props.currentNav==='screen-login'?'current':''} to="/">Login</Link>
                  <Link className={this.props.currentNav==='screen-dashboard'?'current':''} to="/dashboard">Dashboard</Link>
                </header>

                {user}

                <main>
                  <Route exact path="/" component={Login} />
                  <Route exact path="/dashboard" component={requireAuth(Dashboard)} />
                </main>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    user: state.auth.user,
    currentNav: state.nav.currentNav
})

const mapDispatchToProps = dispatch => bindActionCreators({
}, dispatch)

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(App))